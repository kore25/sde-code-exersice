# Platform Science Code Exercise
This app creates a program that assigns each shipment destination to a given driver while maximizing the total suitability of all shipments to all drivers.

## Installation

First, you need to install the last version of [node js](https://nodejs.org/en/). Then you need to run the next command to install all dependencies for the project. Just be sure to be inside of the project to run the command.

```bash
npm i
```

## Run

Then to run the project we need the follow command:

```bash
npm run dev "Path of shipment's file" "Path of driver's file"
```

As you see we need paths to can run correctly the project, inside the project exist these files for example in the data folder, you can use that files to do a test with this command:

```bash
npm run dev ./data/shipments.txt ./data/drivers.txt
```