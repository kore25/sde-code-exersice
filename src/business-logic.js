const driversSelected = [];
const routes = [];

/**
 * This function calculate the destiny for each driver with bussines logic
 * @param {array} shipmentData
 * @param {array} driversData
 * @returns {object} two array, one is the routes and the other is missing drivers list
 */
const calculateRoutes = (shipmentData, driversData) => {
    return new Promise((resolve, reject) => {
        try {
            console.log('Start calculating the best driver for shipment  ...');
            shipmentData.map( async (shipmentLine, index) => {
                const shipmentLength = shipmentLine.length;
                const isEven = shipmentLength % 2 === 0;
                const shipmentCommon = await buildCommonFactors(shipmentLength);
                const bestDriver = await getBestDriver(driversData, shipmentCommon, isEven, driversSelected);
                routes.push({ route: shipmentLine, driver: bestDriver.name, ss: bestDriver.ss });
                if (shipmentData.length - 1 === index){
                    let missingDrivers = []
                    if(driversData.length > shipmentData.length) missingDrivers = await getMissingDrivers(driversData);
                    console.log('The calculation is done.');
                    return resolve({routes, missingDrivers});
                }
            });
        } catch (error) {
            return reject(error);
        }
    });
}

/**
 * This function is bussines logic to define wich driver is the best to the destiny
 * @param {array} driversData
 * @param {boolean} isEven
 * @returns {object} the best driver with ss points
 */
const getBestDriver = (driversData, shipmentCommon, isEven) => {
    return new Promise((resolve, reject) => {
        try {
            const bestDriver = [];
            driversData.map( async (driversLine, index) => {
                // console.log('driversLine -->', driversLine);
                let ss = 0;
                const diverLength = driversLine.length;
                const driversCommon = await buildCommonFactors(diverLength);
                if(!driversSelected.includes(driversLine)){
                    let existsCommonFactors = false;
                    if(driversCommon.length > 0 && shipmentCommon.length > 0){
                        existsCommonFactors = await getCommonFactors(driversCommon, shipmentCommon);
                    }
                    if(isEven){
                        const driverVowelsCount = driversLine.match(/[aeiou]/gi).length;
                        ss =  driverVowelsCount * 1.5;
                    } else {
                        const driverConsonantsCount = driversLine.match(/[^aeiou]/gi).length;
                        ss =  driverConsonantsCount * 1;
                    }
                    if (existsCommonFactors) {
                        const moreFiftyPorcent = ss * 0.50;
                        ss = ss + moreFiftyPorcent;
                    }
                    bestDriver.push({ name: driversLine, ss });
                }
                if (driversData.length - 1 === index){
                    let best = {};
                    if(bestDriver.length > 0){
                        best = bestDriver.reduce((prev, current) =>  prev.ss > current.ss ? prev : current);
                        driversSelected.push(best.name);
                    }
                    return resolve(best);
                }
            });
        } catch (error) {
            return reject(error);
        }
    });
}

/**
 * This function return the missing drivers
 * @param {array} driversData
 * @returns {array} missing drivers
 */
const getMissingDrivers = (driversData) => {
    return new Promise((resolve, reject) => {
        try {
            const missingDrivers = [];
            driversData.map( (driversLine, index) => {
                if(!driversSelected.includes(driversLine)){
                    missingDrivers.push(driversLine);
                }
                if (driversData.length - 1 === index){
                    return resolve(missingDrivers);
                }
            });
        } catch (error) {
            return reject(error);
        }
    });
}

/**
 * This function build common factors
 * @param {number} number
 * @returns {array} common factors
 */
const buildCommonFactors = (number) => {
    return new Promise((resolve, reject) => {
        try {
            const numbersFactors = [];
            const result = [];
            for (let i = 2; i < number; i++) {
                for (let x = number - 1; x > 1; x--) {
                    if((i * x) === number){
                        if (!numbersFactors.find( (nf) => nf.i === x && nf.x === i)){
                            numbersFactors.push({ i, x });
                            result.push(i);
                            result.push(x);
                        }
                    }
                    if (i === number -1 && x === 2){
                        return resolve(result);
                    }
                }
            }
        } catch (error) {
            return reject(error);
        }
    });
}

/**
 * This function get common factors
 * @param {array} driversCommon
 * @param {array} shipmentCommon
 * @returns {boolean} if exists common factors
 */
const getCommonFactors = (driversCommon, shipmentCommon) => {
    return new Promise((resolve, reject) => {
        const commonFactors = [];
        driversCommon.map((driverNumber, i) => {
            const equals = shipmentCommon.find( sc => sc === driverNumber);
            if(equals)
                commonFactors.push(equals);
            if(driversCommon.length - 1 === i) {
                return resolve(commonFactors.length > 0);
            }
        });
    });
}

module.exports = {
    calculateRoutes,
}